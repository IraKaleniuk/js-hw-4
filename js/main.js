"use strict";

const doMathOperations = (fNumber, sNumber, operator) => {
    switch (operator) {
        case "+":
            return +fNumber + +sNumber;
        case "-":
            return fNumber - sNumber;
        case "*":
            return fNumber * sNumber;
        case "/":
            return fNumber / sNumber;
        default:
            return "No such operation"
    }
}

let firstNumber = prompt("Enter first number:");
let secondNumber = prompt("Enter second number:");

while (firstNumber === "" || firstNumber === null || isNaN(firstNumber)
|| secondNumber === "" || secondNumber === null || isNaN(secondNumber)) {
    firstNumber = prompt("Enter first number:", firstNumber);
    secondNumber = prompt("Enter second number:", secondNumber);
}

let mathOperator = prompt("Choose one of +, -, *, / operations and enter it:");

const result = doMathOperations(firstNumber, secondNumber, mathOperator);

if (result !== "No such operation") {
    console.log(`${firstNumber} ${mathOperator} ${secondNumber} = ${result}`);
} else {
    console.log(result);
}



